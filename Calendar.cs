﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Omer_Benedek_Lesson_19
{
    public partial class Calendar : Form
    {
        private string _userName; //username from previews form
        private string _password; //password from previews form
        Dictionary<string, string> dates = new Dictionary<string, string>(); //dates and names will be stored here

        public Calendar()
        {
            InitializeComponent();
        }

        public string Username //gets the username
        {
            get { return _userName; }
            set { _userName = value; birthday.Text = "Choose a date"; }
        }

        public string Password //gets the password
        {
            get { return _password; }
            set { _password = value; }
        }

        private void Calendar_Load(object sender, EventArgs e) //what happens when the form loads
        {
            string filename = _userName + "DB.txt";

            using (StreamReader sr = new StreamReader(filename)) //opens the database
            {
                string line;

                string name = "";
                string date = "";
                int i;

                while ((line = sr.ReadLine()) != null) //gets one line at a time
                {
                    name = "";
                    date = "";
                    for (i = 0; i < line.Length && line[i].ToString() != ","; i++)
                    {
                        name += line[i];
                    }

                    for (i = i + 1; i < line.Length; i++)
                    {
                        date += line[i];
                    }

                    dates.Add(date, name); //adds the data to the dict
                }
            }
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e) //updates the bottom line
        {
            string date = e.End.ToShortDateString();
            if(dates.ContainsKey(date))
            {
                birthday.Text = "Today " + dates[date] + " is celebrating his/her BIRTHDAY!";
            }
            else
            {
                birthday.Text = "No one has a birthday today...";
            }
        }

        private void Calendar_FormClosed(object sender, FormClosedEventArgs e) //red x is pressed
        {
            Environment.Exit(0);
        }
    }
}
