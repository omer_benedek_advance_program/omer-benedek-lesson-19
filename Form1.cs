﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Omer_Benedek_Lesson_19
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //exit button
        private void exit_Click_1(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //sign in button clicked
        private void Signin_Click(object sender, EventArgs e)
        {
            string username = usernameTxt.Text;
            string password = passTxt.Text;

            using (StreamReader sr = new StreamReader("Users.txt")) //opens the file
            {
                string line;
                string usr = "";
                string pass = "";
                int i;
                bool flag = false;
                while ((line = sr.ReadLine()) != null) //gets one line at a time and checks it
                {
                    usr = "";
                    pass = "";
                    for (i = 0; i<line.Length && line[i].ToString() != ","; i++)
                    {
                        usr += line[i];
                    }

                    for(i = i+1; i<line.Length; i++)
                    {
                        pass += line[i];
                    }

                    if(usr == username && pass == password)
                    {
                        flag = true;
                    }
                }
                if(!flag) //worng username/password/both
                {
                    MessageBox.Show("Wrong Username or Password! :(");
                }
                else //both are correct
                {
                    Calendar cla = new Calendar(); //calles the new form
                    this.Hide();
                    cla.Username = username;
                    cla.Password = password;

                    cla.ShowDialog();
                }
            }


        }

        private void Cancel_Click(object sender, EventArgs e) //cancel clicked
        {
            MessageBox.Show("Goodbye!");
            Environment.Exit(0);
        }
    }
}
